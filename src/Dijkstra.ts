import Graph from './Graph';
import Node from './Node';
import Path from './Path';

export default class Dijkstra {
    public graph: Graph;

    public nodeStart: Node;

    public constructor(graph: Graph, nodeStart: Node) {
        this.graph = graph;
        this.nodeStart = nodeStart;
    }

    public init(): void {
        this.graph.nodes.forEach(node => {
            node.visited = false;
            node.bestPath = null;
        });
    }

    public calculate(): void {
        this.init();

        this.nodeStart.bestPath = new Path(0, null);

        let currentNode = this.nodeStart;

        while (currentNode) {
            currentNode.arcs.forEach(arc => {
                const path = new Path(currentNode.bestPath.distance + arc.weight, currentNode);

                if (!arc.nodeTo.bestPath || path.distance < arc.nodeTo.bestPath.distance) {
                    arc.nodeTo.bestPath = path;
                }
            });

            currentNode.visited = true;
            currentNode = null;

            this.graph.nodes
                .filter(node => !node.visited && node.bestPath)
                .forEach(node => {
                    if (null === currentNode || node.bestPath.distance < currentNode.bestPath.distance) {
                        currentNode = node;
                    }
                })
            ;
        }
    }

    /**
     * Once calculated has been called,
     * returns best path to a node,
     * or null if no path to go here.
     */
    public getPathTo(nodeTo: Node): Node[]|null {
        const path: Node[] = [];

        if (!nodeTo.bestPath) {
            return null;
        }

        let node = nodeTo;

        while (node) {
            path.unshift(node);
            node = node.bestPath.nodeFrom;
        }

        return path;
    }
}
