import Arc from './Arc';
import Path from './Path';

export default class Node {
    /**
     * Arcs starting from this node
     */
    public arcs: Arc[] = [];

    public visited: boolean = false;

    public bestPath: Path;

    /**
     * Object that represent this node in your business logic.
     * In example: "A", new Point(x, y), new Square(4, 2), ...
     */
    public payload: any;

    public constructor(payload: any = null) {
        this.payload = payload;
    }

    public getArcTo(node: Node): Arc {
        for (let i = 0; i < this.arcs.length; i++) {
            if (this.arcs[i].nodeTo === node) {
                return this.arcs[i];
            }
        }

        return null;
    }
}
