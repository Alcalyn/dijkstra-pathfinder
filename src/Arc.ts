import Node from './Node';

export default class Arc {
    public nodeTo: Node;

    public weight: number;

    public constructor(nodeTo: Node, weight: number = 1) {
        this.nodeTo = nodeTo;
        this.weight = weight;
    }
}
