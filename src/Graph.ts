import Node from './Node';
import Arc from './Arc';

export default class Graph {

    public nodes: Node[] = [];

    public addNode(node: Node): Graph {
        if (!Object.values(this.nodes).includes(node)) {
            this.nodes.push(node);
        }

        return this;
    }

    public removeNode(node: Node): Graph {
        const index = this.nodes.indexOf(node);

        if (-1 === index) {
            return this;
        }

        this.nodes.splice(index, 1);

        return this;
    }

    public addOrientedArc(nodeFrom: Node, nodeTo: Node, weight: number = 1): Graph {
        this
            .addNode(nodeFrom)
            .addNode(nodeTo)
        ;

        nodeFrom.arcs.push(new Arc(nodeTo, weight));

        return this;
    }

    public removeOrientedArc(nodeFrom: Node, nodeTo: Node): Graph {
        const arc = nodeFrom.getArcTo(nodeTo);

        if (arc) {
            nodeFrom.arcs.splice(nodeFrom.arcs.indexOf(arc), 1);
        }

        return this;
    }

    public addArc(node0: Node, node1: Node, weight: number = 1): Graph {
        this
            .addOrientedArc(node0, node1, weight)
            .addOrientedArc(node1, node0, weight)
        ;

        return this;
    }

    public removeArc(node0: Node, node1: Node): Graph {
        this
            .removeOrientedArc(node0, node1)
            .removeOrientedArc(node1, node0)
        ;

        return this;
    }

    public findNodeByPayload(payload: any): Node {
        return this.nodes
            .find(node => node.payload === payload)
        ;
    }

    public clone(): Graph {
        const graphClone = new Graph();
        const nodeClones: Map<Node, Node> = new Map();

        this.nodes.forEach(nodeSource => {
            const nodeClone = new Node(nodeSource.payload);
            nodeClones.set(nodeSource, nodeClone);
            graphClone.addNode(nodeClone);
        });

        this.nodes.forEach(nodeSource => {
            nodeSource.arcs.forEach(arcSource => {
                graphClone.addOrientedArc(
                    nodeClones.get(nodeSource),
                    nodeClones.get(arcSource.nodeTo),
                    arcSource.weight,
                );
            });
        });

        return graphClone;
    }
}
