import Node from './Node';

export default class Path {
    /**
     * Total distance from starting node.
     */
    public distance: number;

    public nodeFrom: Node;

    public constructor(distance: number, nodeFrom: Node) {
        this.distance = distance;
        this.nodeFrom = nodeFrom;
    }
}
