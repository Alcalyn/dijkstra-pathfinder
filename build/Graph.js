"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Node_1 = require("./Node");
const Arc_1 = require("./Arc");
class Graph {
    constructor() {
        this.nodes = [];
    }
    addNode(node) {
        if (!Object.values(this.nodes).includes(node)) {
            this.nodes.push(node);
        }
        return this;
    }
    removeNode(node) {
        const index = this.nodes.indexOf(node);
        if (-1 === index) {
            return this;
        }
        this.nodes.splice(index, 1);
        return this;
    }
    addOrientedArc(nodeFrom, nodeTo, weight = 1) {
        this
            .addNode(nodeFrom)
            .addNode(nodeTo);
        nodeFrom.arcs.push(new Arc_1.default(nodeTo, weight));
        return this;
    }
    removeOrientedArc(nodeFrom, nodeTo) {
        const arc = nodeFrom.getArcTo(nodeTo);
        if (arc) {
            nodeFrom.arcs.splice(nodeFrom.arcs.indexOf(arc), 1);
        }
        return this;
    }
    addArc(node0, node1, weight = 1) {
        this
            .addOrientedArc(node0, node1, weight)
            .addOrientedArc(node1, node0, weight);
        return this;
    }
    removeArc(node0, node1) {
        this
            .removeOrientedArc(node0, node1)
            .removeOrientedArc(node1, node0);
        return this;
    }
    findNodeByPayload(payload) {
        return this.nodes
            .find(node => node.payload === payload);
    }
    clone() {
        const graphClone = new Graph();
        const nodeClones = new Map();
        this.nodes.forEach(nodeSource => {
            const nodeClone = new Node_1.default(nodeSource.payload);
            nodeClones.set(nodeSource, nodeClone);
            graphClone.addNode(nodeClone);
        });
        this.nodes.forEach(nodeSource => {
            nodeSource.arcs.forEach(arcSource => {
                graphClone.addOrientedArc(nodeClones.get(nodeSource), nodeClones.get(arcSource.nodeTo), arcSource.weight);
            });
        });
        return graphClone;
    }
}
exports.default = Graph;
//# sourceMappingURL=Graph.js.map