import Graph from './Graph';
import Node from './Node';
export default class Dijkstra {
    graph: Graph;
    nodeStart: Node;
    constructor(graph: Graph, nodeStart: Node);
    init(): void;
    calculate(): void;
    /**
     * Once calculated has been called,
     * returns best path to a node,
     * or null if no path to go here.
     */
    getPathTo(nodeTo: Node): Node[] | null;
}
