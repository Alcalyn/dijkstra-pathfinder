import Arc from './Arc';
import Path from './Path';
export default class Node {
    /**
     * Arcs starting from this node
     */
    arcs: Arc[];
    visited: boolean;
    bestPath: Path;
    /**
     * Object that represent this node in your business logic.
     * In example: "A", new Point(x, y), new Square(4, 2), ...
     */
    payload: any;
    constructor(payload?: any);
    getArcTo(node: Node): Arc;
}
