"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Path {
    constructor(distance, nodeFrom) {
        this.distance = distance;
        this.nodeFrom = nodeFrom;
    }
}
exports.default = Path;
//# sourceMappingURL=Path.js.map