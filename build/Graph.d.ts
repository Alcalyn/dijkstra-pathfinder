import Node from './Node';
export default class Graph {
    nodes: Node[];
    addNode(node: Node): Graph;
    removeNode(node: Node): Graph;
    addOrientedArc(nodeFrom: Node, nodeTo: Node, weight?: number): Graph;
    removeOrientedArc(nodeFrom: Node, nodeTo: Node): Graph;
    addArc(node0: Node, node1: Node, weight?: number): Graph;
    removeArc(node0: Node, node1: Node): Graph;
    findNodeByPayload(payload: any): Node;
    clone(): Graph;
}
