import Node from './Node';
export default class Path {
    /**
     * Total distance from starting node.
     */
    distance: number;
    nodeFrom: Node;
    constructor(distance: number, nodeFrom: Node);
}
