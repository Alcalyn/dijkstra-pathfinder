"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Node {
    constructor(payload = null) {
        /**
         * Arcs starting from this node
         */
        this.arcs = [];
        this.visited = false;
        this.payload = payload;
    }
    getArcTo(node) {
        for (let i = 0; i < this.arcs.length; i++) {
            if (this.arcs[i].nodeTo === node) {
                return this.arcs[i];
            }
        }
        return null;
    }
}
exports.default = Node;
//# sourceMappingURL=Node.js.map