import Node from './Node';
export default class Arc {
    nodeTo: Node;
    weight: number;
    constructor(nodeTo: Node, weight?: number);
}
