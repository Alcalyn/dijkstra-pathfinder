"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Arc {
    constructor(nodeTo, weight = 1) {
        this.nodeTo = nodeTo;
        this.weight = weight;
    }
}
exports.default = Arc;
//# sourceMappingURL=Arc.js.map