"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Path_1 = require("./Path");
class Dijkstra {
    constructor(graph, nodeStart) {
        this.graph = graph;
        this.nodeStart = nodeStart;
    }
    init() {
        this.graph.nodes.forEach(node => {
            node.visited = false;
            node.bestPath = null;
        });
    }
    calculate() {
        this.init();
        this.nodeStart.bestPath = new Path_1.default(0, null);
        let currentNode = this.nodeStart;
        while (currentNode) {
            currentNode.arcs.forEach(arc => {
                const path = new Path_1.default(currentNode.bestPath.distance + arc.weight, currentNode);
                if (!arc.nodeTo.bestPath || path.distance < arc.nodeTo.bestPath.distance) {
                    arc.nodeTo.bestPath = path;
                }
            });
            currentNode.visited = true;
            currentNode = null;
            this.graph.nodes
                .filter(node => !node.visited && node.bestPath)
                .forEach(node => {
                if (null === currentNode || node.bestPath.distance < currentNode.bestPath.distance) {
                    currentNode = node;
                }
            });
        }
    }
    /**
     * Once calculated has been called,
     * returns best path to a node,
     * or null if no path to go here.
     */
    getPathTo(nodeTo) {
        const path = [];
        if (!nodeTo.bestPath) {
            return null;
        }
        let node = nodeTo;
        while (node) {
            path.unshift(node);
            node = node.bestPath.nodeFrom;
        }
        return path;
    }
}
exports.default = Dijkstra;
//# sourceMappingURL=Dijkstra.js.map