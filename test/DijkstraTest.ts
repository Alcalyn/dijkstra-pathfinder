import { suite, test } from 'mocha-typescript';
import * as assert from 'assert';
import { Node, Graph, Dijkstra } from '..';

@suite
export default class DijkstraTest {

    @test
    public testCalculate() {
        const nodes: {[key: string]: Node} = {
            A: new Node('A'),
            B: new Node('B'),
            C: new Node('C'),
            D: new Node('D'),
            E: new Node('E'),
            F: new Node('F'),
            G: new Node('G'),
            H: new Node('H'),
        };

        const arcs: [string, number][] = [
            ['AB', 12],
            ['AD', 14],
            ['DE', 10],
            ['CF', 10],
            ['CE', 13],
            ['EF', 16],
            ['FB', 9],
            ['BG', 16],
            ['BH', 21],
            ['GH', 11],
            ['FH', 11],
            ['EH', 10],
        ];

        const graph = new Graph();

        Object.values(nodes).forEach(node => {
            graph.addNode(node);
        });

        arcs.forEach(arc => {
            const arcNodes = arc[0].split('').map(c => nodes[c]);
            graph.addArc(arcNodes[0], arcNodes[1], arc[1]);
        });

        const dijkstra = new Dijkstra(graph, nodes.A);

        dijkstra.calculate();

        const shortestPath = dijkstra.getPathTo(nodes.H).map(node => node.payload);

        assert.deepStrictEqual(shortestPath, ['A', 'B', 'F', 'H']);
    }

    @test
    public testCloneCreatesClonedArcsThatReferenceNodesInClonedGraph() {
        const graph = new Graph();

        const nodes = [
            new Node('A'),
            new Node('B'),
            new Node('C'),
            new Node('D'),
        ];

        graph.addOrientedArc(nodes[0], nodes[1], 10);
        graph.addOrientedArc(nodes[0], nodes[2], 20);
        graph.addOrientedArc(nodes[1], nodes[3], 30);
        graph.addOrientedArc(nodes[2], nodes[2], 40);

        const clonedGraph = graph.clone();
        let arcCount = 0;

        clonedGraph.nodes.forEach(node => {
            node.arcs.forEach(arc => {
                if (!clonedGraph.nodes.includes(arc.nodeTo)) {
                    assert.fail('A node in cloned graph is still an instance from source graph');
                }

                arcCount++;
            });
        });

        assert.equal(arcCount, 4, 'Arcs count changed during clone');
    }

    @test
    public testCalculateBaseExample() {
        const graph = new Graph();

        const A = new Node();
        const B = new Node();
        const C = new Node();
        const D = new Node();
        const E = new Node();

        graph.addArc(A, B);
        graph.addArc(B, C);
        graph.addArc(C, D);
        graph.addArc(A, E);
        graph.addArc(E, D);

        const dijkstra = new Dijkstra(graph, A);

        dijkstra.calculate();

        // Path from A to D
        const pathToD = dijkstra.getPathTo(D);

        assert.equal(pathToD.length, 3);
        assert.equal(pathToD[0], A);
        assert.equal(pathToD[1], E);
        assert.equal(pathToD[2], D);

        // Path from A to C
        const pathToC = dijkstra.getPathTo(C);

        assert.equal(pathToC.length, 3);
        assert.equal(pathToC[0], A);
        assert.equal(pathToC[1], B);
        assert.equal(pathToC[2], C);
    }

    @test
    public testCalculateBaseExampleWithDistance() {
        const graph = new Graph();

        const A = new Node();
        const B = new Node();
        const C = new Node();
        const D = new Node();
        const E = new Node();

        graph.addArc(A, B, 2);
        graph.addArc(B, C, 1);
        graph.addArc(C, D, 1);
        graph.addArc(A, E, 3);
        graph.addArc(E, D, 2);

        const dijkstra = new Dijkstra(graph, A);

        dijkstra.calculate();

        // Path from A to D
        const pathToD = dijkstra.getPathTo(D);

        assert.equal(pathToD.length, 4);
        assert.equal(pathToD[pathToD.length - 1].bestPath.distance, 4);
        assert.equal(pathToD[0], A);
        assert.equal(pathToD[1], B);
        assert.equal(pathToD[2], C);
        assert.equal(pathToD[3], D);

        // Path from A to C
        const pathToC = dijkstra.getPathTo(C);

        assert.equal(pathToC.length, 3);
        assert.equal(pathToC[pathToC.length - 1].bestPath.distance, 3);
        assert.equal(pathToC[0], A);
        assert.equal(pathToC[1], B);
        assert.equal(pathToC[2], C);
    }

    @test
    public testCalculateBaseExampleWithOrientedArcs() {
        const graph = new Graph();

        const A = new Node('A');
        const B = new Node('B');
        const C = new Node('C');
        const D = new Node('D');
        const E = new Node('E');

        graph.addOrientedArc(B, A);
        graph.addArc(B, C);
        graph.addArc(C, D);
        graph.addOrientedArc(A, E);
        graph.addOrientedArc(E, D);

        const dijkstra = new Dijkstra(graph, A);

        dijkstra.calculate();

        // Path from A to D
        const pathToD = dijkstra.getPathTo(D);

        assert.equal(pathToD.length, 3);
        assert.equal(pathToD[0], A);
        assert.equal(pathToD[1], E);
        assert.equal(pathToD[2], D);

        // Path from A to C
        const pathToC = dijkstra.getPathTo(C);

        assert.equal(pathToC.length, 4);
        assert.equal(pathToC[0], A);
        assert.equal(pathToC[1], E);
        assert.equal(pathToC[2], D);
        assert.equal(pathToC[3], C);
    }

    @test
    public testCalculateExampleWithAnyPayload() {
        const graph = new Graph();

        const A = new Node('A (1;1)');
        const B = new Node('B (4;5)');
        const Paris = new Node('Paris');
        const London = new Node('London');
        const FarFarAway = new Node('🏰');

        graph.addArc(A, B, 5);
        graph.addArc(B, Paris);
        graph.addArc(Paris, London);
        graph.addArc(B, London);
        graph.addArc(London, FarFarAway, 40);

        const dijkstra = new Dijkstra(graph, A);

        dijkstra.calculate();

        // Path from A to 🏰
        const pathToD = dijkstra.getPathTo(graph.findNodeByPayload('🏰'));

        assert.equal(pathToD.length, 4);
        assert.equal(pathToD[0].payload, 'A (1;1)');
        assert.equal(pathToD[1].payload, 'B (4;5)');
        assert.equal(pathToD[2].payload, 'London');
        assert.equal(pathToD[3].payload, '🏰');
        assert.equal(pathToD[pathToD.length - 1].bestPath.distance, 46);
    }

    @test
    public testCalculateExampleClonedGraph() {
        const graph = new Graph();

        const A = new Node('A');
        const B = new Node('B');
        const C = new Node('C');

        graph.addArc(A, B);
        graph.addArc(B, C);

        const graph2 = graph.clone();

        graph2.addArc(graph2.findNodeByPayload('A'), graph2.findNodeByPayload('C'));

        // Instanciate a Dijkstra instance with A as starting node.
        const dijkstra = new Dijkstra(graph, A);
        dijkstra.calculate();

        // Path from A to C
        const path = dijkstra.getPathTo(C); // Node[] = [A, B, C]

        assert.equal(path.length, 3);
        assert.equal(path[0].payload, 'A');
        assert.equal(path[1].payload, 'B');
        assert.equal(path[2].payload, 'C');

        // Instanciate another Dijkstra instance on graph2.
        const dijkstra2 = new Dijkstra(graph2, graph2.findNodeByPayload('A'));
        dijkstra2.calculate();

        // Path from A to C
        const path2 = dijkstra2.getPathTo(graph2.findNodeByPayload('C')); // Node[] = [{payload='A'}, {payload='C'}]

        assert.equal(path2.length, 2);
        assert.equal(path2[0].payload, 'A');
        assert.equal(path2[1].payload, 'C');
    }
}
